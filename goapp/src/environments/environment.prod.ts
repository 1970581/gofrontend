export const environment = {
  production: true,
  apiUrl: 'http://http://www.gonogoapp.org',
  register_user_url: 'http://google.com',


  // Users 
  users:{    
    //users_host: "http://localhost:8080",
    users_host: "https://gonogoapp.org:8443",
    login_ask_token_url : "/realms/gonogoapp/protocol/openid-connect/token",
    ask_userinfo_url : "/realms/gonogoapp/protocol/openid-connect/userinfo",
    logout_token_url : "/realms/gonogoapp/protocol/openid-connect/logout",
    keycloak_login_client_id : "gonogo",
    keycloak_login_grant_type : "password",
    keycloak_refresh_token_grant_type :"refresh_token",
    keycloak_register_user_url: "/realms/gonogoapp/account",
    
    rgpd_consent_ok : "on"   

  },

  backend:{
    backend_host: "https://gonogoapp.org:5001",
    save_test_result: "/api/TestResults",
    get_test_results: "/api/TestResults/"
  },

  // Gender construct.
  gender : {
    male: "male",
    female: "female",
    unknown: "unknown"
  },

  // Go/NoGo Test Parameters.
  defaultTestParameters : {
    test_version : "2",
    numberOfTrials : 20, //20,    
    numberOfNoGoTrials: 4, //4,  // number of NoGo should be less than numberOfTrials
    name_go: "go",
    name_nogo: "nogo",
    trial_fail: true,
    trial_pass: false,
    SCORE_GO_FAIL_TIME_TO_RECORD: 2000,
    SCORE_NOGO_TIME_TO_RECORD: 2000,
    AVERAGE_REACTION_TIME_IN_CASE_ALL_GO_FAIL: 5000,

    // Image show times in miliseconds:
    image : {
      WAIT_TIME_TO_SHOW_INSTRUCTIONS: 2000,
      WAIT_TIME_TO_FAIL_GO_CASE: 500,
      WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON: 80,
      WAIT_TIME_TO_PASS_NOGO_CASE: 2000,
      WAIT_TIME_TO_DISPLAY_FAIL: 2000,
      WAIT_TIME_BETWIN_TRIALS: 500
    }
  },

  // Fatigue info
  fatigue : {
    defaultFatigueLevel: 0
  },

  /** Language options - Translations at /assets/i18n/ */
  language : {
    defaultLanguage: "en", 
    languagePt: "pt",
    languageEn: "en"  
    // AppComponent loads the two avaiable languages.  App.Module.ts set the default and in use language.
  },

  // Gecad Cyberfactory prototype model related options.
  gecadbasicfatiguemodel:{

    /** URL to post the gecad result for Gecad*/
    idepa_post_url : "https://gonogoapp.org:5001/api/Gecad",

    /** Get IP URL */    
    ip_get_url : "https://api.ipify.org/?format=json",
    //ip_get_url : "http://api.ipify.org/?format=json",
    // This url is used to obtain an IP address of the sender, watch out that browsers like firefox
    // don't allow Https and http mixed, and will block the call.

    /** URL for the prediction model */
    //direct_prediction_model_url : "http://www.gonogoapp.org:7000/fadiga2a",  // Direct to model
    direct_prediction_model_url : "https://gonogoapp.org:5001/api/TestResults/fadiga2a", // to Backend that calls the model.

    // /**  Function to calculate a fatigue level based on State of The Art for demonstration purposes. */  
    // rateFatigue(age : number, avgReactionTime : number, nGoFail : number, nNoGoFail : number) : number
    // {
    //   console.error("This methods for fatigue estimation should not be used.");
    //   // unfatigated level
    //   let fatigueLevel : number = 1;
    //   // max fatigue level
    //   let max_fatigueLevel :number = 7;
    //   // Reaction time for a 18 year old. (ms)
    //   let freshReactionTime :number = 220;
    //   // Reaction time delay per year old.
    //   let ageImpactInRt = -0.5;
    //   // Reaction time that increases fatigue level by one (ms)
    //   let rtPerLevel = 50; 
      
    //   // Adjust reaction time for age:
    //   let rt : number = 0 ;
    //   if (age <= 18) rt = avgReactionTime; else rt = avgReactionTime + ((age - 18) * ageImpactInRt);
    //   if(rt < freshReactionTime) rt = freshReactionTime;

    //   // get fatigue level from just RT
    //   rt = rt -freshReactionTime;
    //   if (rt < 0) rt = 0;
    //   fatigueLevel = fatigueLevel + Math.floor(rt/rtPerLevel);

    //   // add 1 fatigue level per fail
    //   fatigueLevel = fatigueLevel + nGoFail + nNoGoFail;
    //   // CAP fatigue level.
    //   if(fatigueLevel > max_fatigueLevel) fatigueLevel = max_fatigueLevel;

    //   return fatigueLevel;
    // },

    /** Function to return the description of the fatigue level Gecad/Cyberfactory prototype */
    getFatigueLevelString(level : number, lang : string ) : string
    {
      let answer :string = "";
      if (environment.language.languagePt === lang)
      {
        switch(level){  // Escala em portugues
          case 0:
          case 1:        
            answer = "Fadiga não elevada"; break;
          case 2:
            answer = "Fadiga não elevada"; break;
          case 3:
            answer = "Fadiga não elevada"; break;
          case 4:
            answer = "Fadiga não elevada"; break;
          case 5:
            answer = "Fadiga não elevada"; break;
          case 6:
            answer = "Fadiga elevada"; break;
          default:
            answer = "Fadiga elevada"; break;
        }
      }
      else {  // Escala em Ingles.
        switch (level) {
          case 0:
          case 1:
            answer = "Not Extremely Tired"; break;
          case 2:
            answer = "Not Extremely Tired"; break;
          case 3:
            answer = "Not Extremely Tired"; break;
          case 4:
            answer = "Not Extremely Tired"; break;
          case 5:
            answer = "Not Extremely Tired"; break;
          case 6:
            answer = "Extremely Tired"; break;
          default:
            answer = "Extremely Tired"; break;
        }
      }
      return answer;
    }
    
  }

};
