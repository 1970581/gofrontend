import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewhistComponent } from './hist/viewhist/viewhist.component';
import { AuthguardbaseService } from './Services/authguardbase.service';
import { ViewtrainComponent } from './train/viewtrain/viewtrain.component';
import { LoginComponent} from './user/login/login.component';
import { ViewtestComponent} from './viewtest/viewtest.component'
import { ViewtestfatigueComponent } from './viewtestfatigue/viewtestfatigue.component';

const routes: Routes = [
  //{ path: '', redirectTo: 'login' },
  //{ path: '/', redirectTo: 'login' },
  { path: '', redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},  
  { path: 'test', component: ViewtestComponent},
  // { path: 'testfatigue', component: ViewtestfatigueComponent},
  { path: 'testfatigue', component: ViewtestfatigueComponent,canActivate: [AuthguardbaseService], data: { locktype:"gonogo"}},
  { path: 'hist', component: ViewhistComponent,canActivate: [AuthguardbaseService], data: { locktype:"gonogo"}},
  { path: 'train', component: ViewtrainComponent,canActivate: [AuthguardbaseService], data: { locktype:"gonogo"}}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
