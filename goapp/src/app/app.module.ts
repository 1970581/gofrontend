import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './user/login/login.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ViewtestComponent } from './viewtest/viewtest.component';
import { ViewtestfatigueComponent } from './viewtestfatigue/viewtestfatigue.component';
import { GonogoComponent } from './gonogo/gonogo.component';
import { ShowgecadresultComponent } from './showgecadresult/showgecadresult.component';
import { ShownavbarComponent } from './shownavbar/shownavbar.component';
import { FatigueComponent } from './fatigue/fatigue.component';
import { ShowtestresultComponent } from './showtestresult/showtestresult.component';

import {HttpClientModule, HttpClient} from '@angular/common/http';

/** Multi language Check: https://github.com/ngx-translate/core */
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { environment } from 'src/environments/environment';
import { LangselectorComponent } from './langselector/langselector.component';
import { UserviewComponent } from './user/userview/userview.component';
import { ViewhistComponent } from './hist/viewhist/viewhist.component';
import { ViewtrainComponent } from './train/viewtrain/viewtrain.component';
// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ViewtestComponent,
    ViewtestfatigueComponent,
    GonogoComponent,    
    ShowgecadresultComponent, ShownavbarComponent, FatigueComponent, ShowtestresultComponent, LangselectorComponent, UserviewComponent, ViewhistComponent, ViewtrainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule , ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: environment.language.defaultLanguage,
      useDefaultLang: true
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
