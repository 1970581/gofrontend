import { Injectable } from '@angular/core';
import { environment } from './../../environments/environment';
import {TranslateService} from '@ngx-translate/core';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { TokenUtil } from '../user/tokenutil';
import { User } from '../modelo/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private isLogedIn:boolean = false;
  private static readonly MY_DEFAULT_USERNAME : string = "Unknown";  
  public displayUsername : string = LoginService.MY_DEFAULT_USERNAME;
  public showLogin401Message : boolean = false;

  public access_token:string;
  /** The latest refresh token */
  public refresh_token:string;
  /** The latest user information */
  public myUser:User;
  /** The time the access token last before expiring */
  public access_token_expire_time:number;
  /** The fraction of the expire time we use to request a new token. */
  public readonly RATE_OF_TIME_TO_REQUEST_TOKEN:number = 0.90;
  /** Timeout for token requesting */
  public myTimeout! : any;  // Timeout variable storage
  /** Convertion factor for the timeout time which is in miliseconds. */
  public readonly SECONDS_TO_MILISECONDS : number = 1000;

  constructor(private http: HttpClient, private translate : TranslateService) {
    this.access_token = "";
    this.refresh_token = "";
    this.access_token_expire_time = 1800;
    this.myUser = new User();
   }

  /** Checks if a user is loged in */
  public isAUserLogedIn():boolean{return this.isLogedIn;}

  public doLogin(username: string, password: string): void {
    this.showLogin401Message = false;
    let keycloak_login_url: string = environment.users.users_host + environment.users.login_ask_token_url;
    let client_id: string = environment.users.keycloak_login_client_id;
    let grant_type: string = environment.users.keycloak_login_grant_type;

    this.showLogin401Message = false;

    const myBody = new HttpParams()
      .set(`username`, username)
      .set(`password`, password)
      .set(`client_id`, client_id)
      .set(`grant_type`, grant_type)
      ;
    
    const myHttpOptions = {                 //https://github.com/angular/angular/issues/18586
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
        //, "Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
    };

    this.http.post<Response>(keycloak_login_url, myBody.toString(), myHttpOptions).subscribe(
      {
       next: response  => {
        this.storeKeyCloakLoginToken(response.body);        
       },
       error : error => {
        console.log("Error trying to login user to KeyCloak. See bellow:");
        console.log(error);
        if (error instanceof HttpErrorResponse && error.status == 401) {
          this.showFailedLoginBy401UnauthorizedMesssage();
        } else {
          this.showFailedLoginByUnknownReasonsMessage();   // Lets send a generic warning to the user.
        }        
       }
      });
  }

  /**
   * Stores and processes the login token from keycloak creating the user
   * @param token The received token returned by Keycloak
   */
   private storeKeyCloakLoginToken(token:any){    
    // Validate the information structure.
    if(!TokenUtil.validateAccessToken(token)){
       this.showMalfomedAccessTokenMessage();
       return;
    }
    
    this.access_token = token.access_token;
    this.refresh_token = token.refresh_token;  
    this.access_token_expire_time = token.expires_in;  
    console.log("Got login token." + new Date().toISOString());       
    
    this.scheduleTokenRefresh();
    this.myUser = TokenUtil.buildUser(token);
    this.displayUsername = this.myUser.username;

    // TODO: every token refresh, it will reset the language to this. Must improve.
    this.updateLanguageOnLogin();
    this.isLogedIn = true;    
  }


  /** Presents a alert message in case login fails due to unknown reasons*/
  public showFailedLoginByUnknownReasonsMessage(): void {
    let message_login_failed: string = this.translate.instant("LOGIN.MESSAGE.FAILED_UNKNOWN");
    alert(message_login_failed);    
  }

  /** Presents a alert message in case login fails due to 401 Unauthorized */
  public showFailedLoginBy401UnauthorizedMesssage() {
    let message_login_failed: string = this.translate.instant("LOGIN.MESSAGE.FAILED_401"); //"unauthorized";
    //alert(message_login_failed);
    this.showLogin401Message = true;
  }

  /** Presents an alert message in case the access token received is malformed. */
  public showMalfomedAccessTokenMessage() {
    let message_malformed_token: string = this.translate.instant("LOGIN.MESSAGE.TOKEN_MALFORMED");
    alert(message_malformed_token);
  }

    /**  Message the user with information that token refresh failed and that he is being loged out. */
    public failedToRefreshLoginToken():void{
      let message_refresh_token_login_failed:string = this.translate.instant("LOGIN.MESSAGE.TOKEN_REFRESH_FAILED");
      alert(message_refresh_token_login_failed);      
    }

  public redirectToRegistrationUrl():string {
    return environment.users.users_host + environment.users.keycloak_register_user_url;
  }


  /** Logs user out. */  
  public doLogout(): void {
    this.isLogedIn = false;
    this.displayUsername = LoginService.MY_DEFAULT_USERNAME;

    clearTimeout(this.myTimeout);
    this.askKeycloakToLogoutToken(this.refresh_token);
    this.access_token = "";
    this.isLogedIn = false;
    this.refresh_token = "";
    this.myUser = new User();
  }


  /** Schedules the refreshing of the token at a posterior time */
  public scheduleTokenRefresh():void{
    if(this.access_token_expire_time == null || this.access_token_expire_time <0) return;
    let myTimeWait:number = this.access_token_expire_time * this.RATE_OF_TIME_TO_REQUEST_TOKEN * this.SECONDS_TO_MILISECONDS;
    //console.log(""+myTimeWait);
    this.myTimeout = setTimeout(() => {
      this.askRefreshToken();
    }      , myTimeWait);       

  }

  /** This contacts keycloak to ask for a new token using the refresh token, refreshing the user info. */
  public askRefreshToken() {
    //https://stackoverflow.com/questions/51386337/refresh-access-token-via-refresh-token-in-keycloak

    let keycloak_login_url: string = environment.users.users_host + environment.users.login_ask_token_url;

    const myBody = new HttpParams()
      .set(`client_id`, environment.users.keycloak_login_client_id)
      .set(`grant_type`, environment.users.keycloak_refresh_token_grant_type)
      .set(`refresh_token`, this.refresh_token)
      ;

    const myHttpOptions = {                //https://github.com/angular/angular/issues/18586
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
        //, "Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
    };

    this.http.post<Response>(keycloak_login_url, myBody.toString(), myHttpOptions).subscribe(
      {
        next: (response) => {
          this.storeKeyCloakLoginToken(response.body);
        },
        error: (error) => {
          console.log("Error trying to refresh the token to KeyCloak. See bellow:")
          console.log(error);
          this.doLogout();
          this.failedToRefreshLoginToken();   // Lets send a warning to the user.
        }
      }
    )
  }


      /**
   * Calls keycloak to ask them to log out the user/tokens.
   * @param the_refresh_token The refresh token from the user to log out 
   */
  public askKeycloakToLogoutToken(the_refresh_token: string) :void {

    let keycloak_logout_url: string = environment.users.users_host + environment.users.logout_token_url;

    const myBody = new HttpParams()      
      .set(`client_id`, environment.users.keycloak_login_client_id)
      //.set(`grant_type`, environment.users.keycloak_refresh_token_grant_type)
      .set(`refresh_token`, the_refresh_token)
      ;

    //let myBearer :string = "Bearer " + the_access_token;  // We don't need the bearer token and this avoids CORS problems.

    //https://github.com/angular/angular/issues/18586
    const myHttpOptions = {
      headers: new HttpHeaders({
        //'Authorization': myBearer,
        'Content-Type': 'application/x-www-form-urlencoded'
        //,"Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
    };

    this.http.post<Response>(keycloak_logout_url, myBody.toString(), myHttpOptions).subscribe(
      {
        next: (response) => {
          console.log("Sucesefully logged out the token with keycloak.");
        }
        , error: (error: any) => {
          console.log("Error trying to refresh to logout the user/token from KeyCloak. No action required. It will expire naturaly.")
          console.log(error);
        }
      }
    )

  }

  public updateLanguageOnLogin(){
    let myUserLang = this.myUser.lingua;

    if (myUserLang === environment.language.languageEn ){
      this.translate.use(environment.language.languageEn);      
    }    
    else {
      this.translate.use(environment.language.languagePt);      
    } 
    console.log("Changed langua to " + this.translate.currentLang);
  }

}
