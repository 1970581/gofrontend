import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})

/**
 * Class responsible for doing the check if the routes should be open, or if the user should
 * be redirected to the login page.
 */
export class AuthguardbaseService {

  constructor(private router: Router, private loginService: LoginService) { }

  canActivate(route: import("@angular/router").ActivatedRouteSnapshot, state: import("@angular/router").RouterStateSnapshot): boolean | import("@angular/router").UrlTree | import("rxjs").Observable<boolean | import("@angular/router").UrlTree> | Promise<boolean | import("@angular/router").UrlTree> {    
    let goToLogin:boolean = false;
    console.log(route.data);
    if (!this.loginService.isAUserLogedIn()) goToLogin = true;

    // We can implement our user roles requirements here, if needed in future.
    // console.log(this.loginService.myUser);
    // if(route.data.locktype == "eq" && !this.loginService.myUser.roles.includes("user")) goToLogin = true;
    

    if(goToLogin)this.router.navigate(['/login']);
    return true;
  }

}
