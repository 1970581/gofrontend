import { TestBed } from '@angular/core/testing';

import { AuthguardbaseService } from './authguardbase.service';

describe('AuthguardbaseService', () => {
  let service: AuthguardbaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthguardbaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
