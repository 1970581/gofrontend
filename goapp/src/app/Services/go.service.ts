import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';
import { ScoreItem } from '../modelo/scoreitem';
import { TestResult } from '../modelo/testresult';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from './login.service';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ModelPrediction } from '../modelo/modelprediction';


@Injectable({
  providedIn: 'root'
})
export class GoService {


   // This values must be exact in GonogoComponent and in the GoService to avoid Circular dependencys.
  // If changed here you must also change in the other. 
  public static readonly SCORE_GO_TRIAL_TYPE : string = environment.defaultTestParameters.name_go; //"go";
  public static readonly SCORE_NOGO_TRIAL_TYPE : string = environment.defaultTestParameters.name_nogo; //"nogo";
  public static readonly SCORE_TRIAL_FAIL: boolean = environment.defaultTestParameters.trial_fail; // true
  public static readonly SCORE_TRIAL_PASS: boolean = environment.defaultTestParameters.trial_pass; // pass
  /** Value to set the average in case all go trials fail. */
  public static readonly AVERAGE_REACTION_TIME_IN_CASE_ALL_GO_FAIL = environment.defaultTestParameters.AVERAGE_REACTION_TIME_IN_CASE_ALL_GO_FAIL;

  /** last gonogo test performed */
  public lastTestResult : TestResult;

  /** Used to inform that we have a new test result. */
  public observedSubjectTestResult : Subject<TestResult> = new Subject<TestResult>();

  /** Used to inform that we have a new fatigue scale value. */
  public observedSubjectTestResultWithFatigue : Subject<TestResult> = new Subject<TestResult>();

  /** Used to inform that we have posted with sucess a new test Result to backend */
  public observedSubjectTestResultPosted :  Subject<boolean> = new Subject<boolean>();


  constructor(private http: HttpClient, private translate : TranslateService, private loginService: LoginService) {
    this.lastTestResult = new TestResult();
   }

  /**
 * Used to submit a test score of the trials that was just performed but not processed.
 * And warn the View that we got a test result.
 * @param testScoreItemsArray a ScoreItem array of trials, the result of the test from GonogoComponent
 */
  public processNewScore(testScoreItemsArray: ScoreItem[]): void {

    let testResult = new TestResult();
    testResult.replaceScoreItemsArrayByCloning(testScoreItemsArray);

    testResult.userId = this.loginService.myUser.userId;  
    testResult.username = this.loginService.myUser.username;
    testResult.gender = this.loginService.myUser.gender;
    testResult.age = this.loginService.myUser.getAge();
    testResult.rgpd = this.loginService.myUser.rgpd;
    testResult.timestamp = new Date().toISOString();

    // Some values calculations.
    let go_type: string = GoService.SCORE_GO_TRIAL_TYPE;
    let nogo_type: string = GoService.SCORE_NOGO_TRIAL_TYPE;

    let pass_type: boolean = GoService.SCORE_TRIAL_PASS;
    let fail_type: boolean = GoService.SCORE_TRIAL_FAIL;

    let number_of_go: number = 0;
    let number_of_nogo: number = 0;
    let number_of_go_fail: number = 0;
    let number_of_nogo_fail: number = 0;

    let i: number = 0;
    for (i = 0; i < testResult.score.length; i++) {
      if (testResult.score[i].type == go_type) number_of_go++;
      if (testResult.score[i].type == go_type && testResult.score[i].isFail == fail_type) number_of_go_fail++;
      if (testResult.score[i].type == nogo_type) number_of_nogo++;
      if (testResult.score[i].type == nogo_type && testResult.score[i].isFail == fail_type) number_of_nogo_fail++;
    }

    // number of trials
    testResult.gotrialnumber = number_of_go;
    testResult.nogotrialnumber = number_of_nogo;

    // number of fails
    testResult.gofailnumber = number_of_go_fail;
    testResult.nogofailnumber = number_of_nogo_fail;


    // Percentage of fails.
    testResult.gofailpercentage = 0;
    if (number_of_go > 0) testResult.gofailpercentage = (number_of_go_fail / number_of_go);
    testResult.nogofailpercentage = 0;
    if (number_of_nogo > 0) testResult.nogofailpercentage = (number_of_nogo_fail / number_of_nogo);

    testResult.goavg = this.calculateScoreAverage(testResult.score);

    this.lastTestResult = testResult;

    // Inform that we have a test result.
    this.observedSubjectTestResult.next(testResult);

  }

  /**
   *  Calculates the average reaction time of a gonogo test score.
   * @param scoreItemArray ScoreItem Array to calculate the average reaction time.
   */
   public calculateScoreAverage(scoreItemArray : ScoreItem[]) : number {
    if (scoreItemArray == null || scoreItemArray == undefined || scoreItemArray.length == 0) return 0;
    let average : number = GoService.AVERAGE_REACTION_TIME_IN_CASE_ALL_GO_FAIL;
    let timeSum : number = 0;
    let denominator : number = 0; // Number of passes
    for (let i = 0; i < scoreItemArray.length; i++){
      if(scoreItemArray[i].type == GoService.SCORE_GO_TRIAL_TYPE 
        && scoreItemArray[i].isFail == GoService.SCORE_TRIAL_PASS){
          timeSum = timeSum + scoreItemArray[i].time;
          denominator = denominator +1;
        }
    }
    if (denominator <= 0) return average; // Which should be GoService.AVERAGE_REACTION_TIME_IN_CASE_ALL_GO_FAIL;
    average = Math.floor(timeSum / denominator);      
    return average;
  }

  // /**
  //  * Rate Fatigue using Gecad Basic Fatigue Model for demonstration purposes.
  //  * @param age number, int
  //  * @param gender string, unused for now, placeholder
  //  */
  // public rateFatigueUsingGecadBasicFatigueModel(age: number, gender: string) {
  //   let fatigueLevel: number = environment.fatigue.defaultFatigueLevel;
  //   if (this.lastTestResult != null) {
  //     fatigueLevel = environment.gecadbasicfatiguemodel.rateFatigue
  //       (
  //         this.lastTestResult.age,
  //         this.lastTestResult.goavg,
  //         this.lastTestResult.gofailnumber,
  //         this.lastTestResult.nogofailnumber
  //       );
  //     this.lastTestResult.predictedFatigueLevel = fatigueLevel;
  //     this.lastTestResult.isRated = true;
  //   }
  // }

  /**
   * Rate Fatigue using a direct connection to the model.
   * @param age number, int
   * @param gender string, unused for now, placeholder
   */
  public getDirectModelPrediction(age: number, gender: string){

    let the_model_url: string = environment.gecadbasicfatiguemodel.direct_prediction_model_url;

    let body : ModelPrediction = new ModelPrediction();
    body.age = age;
    body.gender = gender;
    body.goavg = this.lastTestResult.goavg;
    body.gofailnumber = this.lastTestResult.gofailnumber;
    body.nogofailnumber = this.lastTestResult.nogofailnumber;
    
    
    //https://github.com/angular/angular/issues/18586
    const myHttpOptions = {
      headers: new HttpHeaders({
 
        //'Content-Type': 'application/x-www-form-urlencoded'
        "Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
      //observe: 'TestResult' as 'body'
    };


    this.http.post<HttpResponse<ModelPrediction>>(the_model_url, body , myHttpOptions).subscribe(
      {
        next: (response) => {
          console.log("Got a direct prediction from the model.");

          // Got a response from Backend, need to update our current data.
          if(response != null && response != undefined && response.body != null && response.body != undefined )
          {            
            this.lastTestResult.isRated = true;
            this.lastTestResult.predictedFatigueLevel = response.body.predictedFatigueLevel;
          }
          else{
            console.log("Error casting to ModelPrediction from direct prediction from the model.")
          }

          this.observedSubjectTestResultPosted.next(true);    
        }
        , error: (error: any) => {
          console.log("Error trying to post direct prediction from the model.")
          console.log(error);

          if (error instanceof HttpErrorResponse && error.status == 401) {
            console.error("Http post Test Result Error. 401 Unauthorised.");
            let message: string = this.translate.instant("BACKEND.ERROR_442");    
            alert(message);
          } else {
            console.error("Http post Test Result Error. Unknown error.");
            let message: string = this.translate.instant("BACKEND.ERROR_UNKNOWN");    
            alert(message);
          }          
          this.observedSubjectTestResultPosted.next(false);    
        }
        , complete: () => {
          console.log("Http post direct prediction from the model, on complete.");
        }
      }
    )

  }


  /** Gets the description for the Gecad Basic Fatigue Model at a certain level. */
  public getFatigueLevelDescription() : string{
    if(this.lastTestResult != null)
    {
      let lang :string = this.translate.currentLang;
      return environment.gecadbasicfatiguemodel.getFatigueLevelString(this.lastTestResult.predictedFatigueLevel, lang);
    }    
    else return "Error! No test result found.";
  }


  /**
   * Processes into the existing GoTestResult the fatige value and returns the GoTestResult 
   * through the subject
   * @param fatigueValue the fatigue value to place in the GoTestResult 
   */
   public processNewFatigueScaleValue(fatigueValue : number): void{
    // Validation
    if( typeof fatigueValue != 'number' ){      
      console.error("GoService got a fatigue value that isn't a number: ");
      console.error(fatigueValue);
      return;
    }

    if (this.lastTestResult == null || this.lastTestResult == undefined ) {
      console.log("GoService got a new fatigue scale: " + fatigueValue+ " , but has no GoTestResult");
      return; 
    }

    this.lastTestResult.selfFatigueLevel = fatigueValue;
    
    this.observedSubjectTestResultWithFatigue.next(this.lastTestResult);
  }


  public submitTestResultToBackend(){
    let backend_url: string = environment.backend.backend_host + environment.backend.save_test_result;

    let body : TestResult = this.lastTestResult;
    let myUsername = this.loginService.myUser.username;
    let myToken = this.loginService.access_token;    

    //https://github.com/angular/angular/issues/18586
    const myHttpOptions = {
      headers: new HttpHeaders({
        'username': myUsername
        ,'token' : myToken
        //'Content-Type': 'application/x-www-form-urlencoded'
        ,"Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
      //observe: 'TestResult' as 'body'
    };


    this.http.post<HttpResponse<TestResult>>(backend_url, body , myHttpOptions).subscribe(
      {
        next: (response) => {
          console.log("Sucesefully saved TestResult.");

          // Got a response from Backend, need to update our current data.
          if(response != null && response != undefined && response.body != null && response.body != undefined )
          {            
            this.lastTestResult.isRated = response.body.isRated;
            this.lastTestResult.predictedFatigueLevel = response.body.predictedFatigueLevel;
          }
          else{
            console.log("Error casting to TestResult from backend answer.")
          }

          this.observedSubjectTestResultPosted.next(true);    
        }
        , error: (error: any) => {
          console.log("Error trying to post TestResult to backend.")
          console.log(error);

          if (error instanceof HttpErrorResponse && error.status == 401) {
            console.error("Http post Test Result Error. 401 Unauthorised.");
            let message: string = this.translate.instant("BACKEND.ERROR_442");    
            alert(message);
          } else {
            console.error("Http post Test Result Error. Unknown error.");
            let message: string = this.translate.instant("BACKEND.ERROR_UNKNOWN");    
            alert(message);
          }          
          this.observedSubjectTestResultPosted.next(false);    
        }
        , complete: () => {
          console.log("Http post Test Result on complete.");
        }
      }
    )

  }
}
