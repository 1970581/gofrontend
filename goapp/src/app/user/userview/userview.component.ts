import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from 'src/app/Services/login.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-userview',
  templateUrl: './userview.component.html',
  styleUrls: ['./userview.component.css']
})
export class UserviewComponent implements OnInit {

  public male : string = environment.gender.male;
  public female : string = environment.gender.female;
  public unknown : string = environment.gender.unknown;
  public lingua_pt : string = environment.language.languagePt;
  public lingua_en : string = environment.language.languageEn;
  public rgpd_concent_ok : string = environment.users.rgpd_consent_ok;

  constructor(public loginService: LoginService, translate : TranslateService)  { }

  ngOnInit(): void {    
    
  }

}
