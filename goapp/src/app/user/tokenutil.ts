import { environment } from "src/environments/environment";
import { User } from "../modelo/user";

export class TokenUtil {
     
    /**
     * Validates if an access token as all the fields to be expects. If not, return false indicating a malformed user. 
     * @param token_complete 
     * @returns true if token is valid.
     */    
    public static validateAccessToken(token_complete : any):boolean{
        
        // Uncode the access token     // split_access_token
        var token = JSON.parse(atob(token_complete.access_token.split('.')[1]));

        if(token.sub == null || token.sub == undefined) return false;
        if(token.preferred_username == null || token.preferred_username == undefined) return false;
        
        if(token.gender == null || token.gender == undefined) return false;
        if(token.birthdate == null || token.birthdate == undefined) return false;

        if(token.lingua == null || token.lingua == undefined) return false;
        if(token.rgpd == null || token.rgpd == undefined) return false;

        // LEGACY
        //if(token.name == null || token.name == undefined) return false;
        //if(token.email == null || token.email == undefined) return false;
        //if(token.realm_access == null || token.realm_access == undefined) return false;
        //if(token.realm_access.roles == null || token.realm_access.roles == undefined) return false;

        return true;
    }


    /**
     * Builds a user from the token info
    * @param token complete Keycloak token 
    */
    public static buildUser(token_complete: any): User {

        var token = JSON.parse(atob(token_complete.access_token.split('.')[1]));        

        let user: User = new User();
        user.userId = token.sub;
        user.username = token.preferred_username;
        user.birthdate = token.birthdate;
        user.gender = token.gender;
        user.lingua = token.lingua;
        user.rgpd = token.rgpd;

        return user;
    }
}