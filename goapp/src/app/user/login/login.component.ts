import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../Services/login.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    /** Username field in the input box */
    public myUsername : string ;
    /** Password field in the input box */
    public myPassword : string ;

    public registerUrl : string;
    public forgetPassword : string;


  constructor(public loginService: LoginService, translate : TranslateService) {
    this.myUsername = "";
    this.myPassword = "";
    this.registerUrl = this.loginService.redirectToRegistrationUrl();
    this.forgetPassword = this.loginService.redirectToRegistrationUrl();
   }

  ngOnInit(): void {        
  }

  public isAUserLogedIn(){
    return this.loginService.isAUserLogedIn();
  }

  public doLogin(myUser : string, myPassword : string): void{
    let pass:string = this.myPassword.toString();
    let username:string = this.myUsername.toString();
    this.loginService.doLogin(username, pass);
    this.myPassword = "";
  }

  /** Asks the login service to do the logout of the loged in user.  */
  public doLogout(){    
    this.loginService.doLogout();
  }

  /** Redirect user to registration page. */
  public redirectToRegistration(){
    let url :string = this.loginService.redirectToRegistrationUrl();
    window.open(url);
  }

}
