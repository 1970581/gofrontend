import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GoService } from '../Services/go.service';

@Component({
  selector: 'app-showgecadresult',
  templateUrl: './showgecadresult.component.html',
  styleUrls: ['./showgecadresult.component.css']
})
export class ShowgecadresultComponent implements OnInit {

  Math = Math;

  public fatigeText : string = "Processing!!!!";

  constructor(public goService : GoService, translate : TranslateService) { }

  ngOnInit(): void {
    this.fatigeText = this.goService.getFatigueLevelDescription();
  }

}
