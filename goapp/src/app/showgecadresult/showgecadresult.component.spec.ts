import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowgecadresultComponent } from './showgecadresult.component';

describe('ShowresultComponent', () => {
  let component: ShowgecadresultComponent;
  let fixture: ComponentFixture<ShowgecadresultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowgecadresultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowgecadresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
