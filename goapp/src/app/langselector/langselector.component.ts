import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-langselector',
  templateUrl: './langselector.component.html',
  styleUrls: ['./langselector.component.css']
})
export class LangselectorComponent implements OnInit {

  public newLanguage :string = environment.language.languagePt

  constructor(private translate : TranslateService) { }

  ngOnInit(): void {
    if (this.translate.currentLang === environment.language.languageEn )
    this.newLanguage = environment.language.languagePt;
    else this.newLanguage = environment.language.languageEn;
  }

  public selectNewLanguage(): void{
    if (this.translate.currentLang === environment.language.languageEn ){
      this.translate.use(environment.language.languagePt);
      this.newLanguage = environment.language.languageEn;
    }    
    else {
      this.translate.use(environment.language.languageEn);
      this.newLanguage = environment.language.languagePt;
    } 
    console.log("Changed langua to " + this.translate.currentLang);
    //TODO: Reboot screen.
  }

}
