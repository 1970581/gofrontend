import { Timestamp } from "rxjs";
import { environment } from "src/environments/environment";
import { ScoreItem } from './scoreitem';

/** Class for information or score about a trial of Go or Nogo */
export class TestResult {
    /** user id of the user */
    public userId: string;
    /** username of the user */
    public username: string;
    /** RGPD status, should be ON if acepted */
    public rgpd : string; 
    /** Date as ISO string with format "2020-02-11T17:05:18.721Z" and special naming for elasticSearch */
    //"@timestamp": string;
    public timestamp : string;
    /** age of user in years */
    public age: number ;
    /** male or female, unknown by default */
    public gender: string ;
    /** version of the go nogo test, for allowing queries on model change */
    public version : string;
    /** fatigue level from scale 1 to 7, zero is unknown */
    public selfFatigueLevel : number;
    /** true if we have rated the fatigue level, default false */
    public isRated : boolean = false;
    /**  Fatigue level predicted by the detection model */
    public predictedFatigueLevel : number;
    
    /** number of go trials done in the test */
    public gotrialnumber : number;
    /** number of nogo trials done in the test */
    public nogotrialnumber : number;
    /** number of go trials failed */
    public gofailnumber : number;
    /** number of nogo trials failed */
    public nogofailnumber : number;
    /** rate of go fails, value goes from 0.0 to 1.0 , rate so we can compare with other version*/
    public gofailpercentage : number;     // rate or factor 0-1
    /** rate of nogo fails, value goes from 0.0 to 1.0 , rate so we can compare with other version*/
    public nogofailpercentage : number;       // rate or factor 0-1
    /** average reaction time in milisecond for passing go trials, huge value indicates that all go trials failed */
    public goavg : number;         // miliseconds
    /** raw data for each of the trials of go or nogo */
    score : ScoreItem[] = new Array<ScoreItem>();



    constructor(){
        this.userId = "";
        this.username = "";
        this.rgpd = "";
        this.timestamp = new Date().toISOString();
        this.age = 0;
        this.gender = environment.gender.unknown;
        this.version = environment.defaultTestParameters.test_version;
        this.selfFatigueLevel = environment.fatigue.defaultFatigueLevel;
        this.predictedFatigueLevel = environment.fatigue.defaultFatigueLevel;

        this.gotrialnumber = 0;
        this.nogotrialnumber = 0;
        this.gofailnumber = 0;
        this.nogofailnumber = 0;
        this.gofailpercentage = 0;
        this.nogofailpercentage = 0;    
        this.goavg = 0;
    }

    /**
     * Reset the array and clones each of the ScoreItem into the score array.
     * @param scoreItemsArray score item array to place into this GoTestResult
     */
    public replaceScoreItemsArrayByCloning(scoreItemsArray : ScoreItem[]) : void {
        this.score = new Array<ScoreItem>();
        let i : number = 0;
        for(i = 0; i < scoreItemsArray.length; i++){
            let si = new ScoreItem();
            si.isFail = scoreItemsArray[i].isFail;
            si.time = scoreItemsArray[i].time;
            si.type = scoreItemsArray[i].type;
            this.score.push(si);
        }
    }       


}