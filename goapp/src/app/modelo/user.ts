import { environment } from "src/environments/environment";

export class User {
    public userId:string = "";
    public username:string = "unknown";    
    public gender:string = environment.gender.unknown;
    public birthdate:string = "1900-01-01";
    public lingua:string = environment.language.defaultLanguage;
    public rgpd : string = ""; //environment.users.rgpd_consent_ok;

    public getAge() : number{
        let age :number = 0;

        try{
            let birth = new Date(this.birthdate);
            let today = new Date();    
            // Aproximate age, not exact to prevent change of age during data colection.
            age = today.getFullYear() - birth.getFullYear();            
        }
        catch {
            console.error("Error processing birthdate." );
            console.log(this.birthdate);
            // Do nothing.
        }

        return age;

    }
}