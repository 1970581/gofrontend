/** Class for GECAD Direct model interaction. Only for GECAD/Cyberfactory. */
export class ModelPrediction {
    
    age : number = 0;    
    gender : string = "male";    
    gofailnumber: number = 0;
    nogofailnumber: number = 0;
    goavg : number =0;
    predictedFatigueLevel : number = 0;
}