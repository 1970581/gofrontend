/** Class for information or score about a trial of Go or Nogo */
export class ScoreItem {
    /** type of trial, go or nogo*/
    type : string = "";
    /** time in miliseconds of the trial */
    time : number = 0;    
    /** true if the trial failed  */
    isFail : boolean = false;
}