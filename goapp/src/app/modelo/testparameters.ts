import { environment } from "src/environments/environment";

export class TestParameters {
    
    public static readonly version :string = environment.defaultTestParameters.test_version;
    public static readonly numberOfTrials : number = environment.defaultTestParameters.numberOfTrials;
    public static readonly numberOfNoGoTrials : number = environment.defaultTestParameters.numberOfNoGoTrials;
    public static readonly waitTimeToFailGo : number = environment.defaultTestParameters.image.WAIT_TIME_TO_FAIL_GO_CASE;
    public static readonly waitTimeToFailGoPressToSoon : number = environment.defaultTestParameters.image.WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON;
    public static readonly waitTimeToPassNoGo : number = environment.defaultTestParameters.image.WAIT_TIME_TO_PASS_NOGO_CASE;
    public static readonly waitTimeBetweenTrials : number = environment.defaultTestParameters.image.WAIT_TIME_BETWIN_TRIALS;
}