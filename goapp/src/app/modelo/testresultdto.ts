/** Class for information or score about a trial of Go or Nogo */
export class TestResultDTO {

    public timestamp : string ="";
    public selfFatigueLevel : string = "L0"; 
    public predictedFatigueLevel : string = "-";
    public gofailnumber : number = 0;
    public nogofailnumber : number = 0;
    public goavg : number = 0;         // miliseconds

    public fatigueLevelDescription : string = "";

    public constructor(){}
}