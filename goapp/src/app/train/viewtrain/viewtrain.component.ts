import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GoService } from 'src/app/Services/go.service';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/Services/login.service';

@Component({
  selector: 'app-viewtrain',
  templateUrl: './viewtrain.component.html',
  styleUrls: ['./viewtrain.component.css']
})
export class ViewtrainComponent implements OnInit, OnDestroy {

  /** go service subscription to receive warnings and results */
  public goTestResultSubjectSubscription! : Subscription; 

  public hasResults : boolean= false;

  constructor(private goService : GoService, private route: ActivatedRoute,translate : TranslateService, public loginService: LoginService) { }

  ngOnInit(): void {
    this.goTestResultSubjectSubscription = this.goService.observedSubjectTestResult.subscribe(
      testResult => { this.hasResults = true; }
    );
    this.hasResults= false;    
  }

  ngOnDestroy(): void {
    this.goTestResultSubjectSubscription.unsubscribe();
  }

  public restartTest(){
    console.log("Restart viewtrain");
    this.hasResults = false;
  }
}
