import { Component, OnDestroy, OnInit } from '@angular/core';
import { GoService } from '../Services/go.service';
import { Subscription } from 'rxjs';
import { TestResult } from '../modelo/testresult';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GecadTest } from '../modelo/gecadtest';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-viewtest',
  templateUrl: './viewtest.component.html',
  styleUrls: ['./viewtest.component.css']
})


//    ,ad8888ba,   88888888888  ,ad8888ba,         db         88888888ba,    
//    d8"'    `"8b  88          d8"'    `"8b       d88b        88      `"8b   
//    d8'            88         d8'                d8'`8b       88        `8b  
//    88             88aaaaa    88                d8'  `8b      88         88  
//    88      88888  88"""""    88               d8YaaaaY8b     88         88  
//    Y8,        88  88         Y8,             d8""""""""8b    88         8P  
//    Y8a.    .a88  88          Y8a.    .a8P  d8'        `8b   88      .a8P   
//     `"Y88888P"   88888888888  `"Y8888Y"'  d8'          `8b  88888888Y"'  


/** Simplified test component for Gecad Cyberfactory*/
export class ViewtestComponent implements OnInit, OnDestroy {

  /** If we should show the Age and Gender selector */
  public showAgeGenderSelector : boolean = true;
  /** If we should show the gonogo test component HTML DIV */
  public showGonogoTestDiv: boolean = false;
  /** If we should show the result */
  public showResultDiv: boolean = false;

  /** Form object for gender. More information on: //https://angular.io/guide/reactive-forms */
  public genderAgeForm : FormGroup ;
  public optionMale : string = environment.gender.male;
  public optionFemale : string = environment.gender.female;
  public regexGenderMatching : string = "^" + environment.gender.male + "$|^" + environment.gender.female + "$";
  /** for age.  */
  public static readonly INVALID_START_AGE_FORM_LEVEL : number = 0;
  public static readonly MIN_AGE_FORM_LEVEL : number = 1;
  public static readonly MAX_AGE_FORM_LEVEL : number = 100;

  public selectedGender :string = environment.gender.unknown;
  public selectedAge : number = 0;
  public capturedIP : string = "";


  /** go service subscription to receive warnings and results */
  public goTestResultSubjectSubscription! : Subscription;
  /** Warn us when the Go service receives the prediction from the model */
  public goTestPredictionArrivedSubjectSubscription! : Subscription;
  
  constructor(private goService : GoService, private route: ActivatedRoute,translate : TranslateService,private http: HttpClient) {
    // Initializate the forms
    this.genderAgeForm = new FormGroup( 
      {
        genderValueController : new FormControl( "",
          [ // Form validators
            Validators.required,
            Validators.pattern(this.regexGenderMatching)            
          ]),
        ageValueController : new FormControl( ViewtestComponent.INVALID_START_AGE_FORM_LEVEL,
        [ // Form validators
          Validators.required,
          Validators.min(ViewtestComponent.MIN_AGE_FORM_LEVEL),
            Validators.max(ViewtestComponent.MAX_AGE_FORM_LEVEL)           
        ])
      }
    );        
  }  

  ngOnInit() {

    // WArning for when we get a processed GoNoGo test result.
    this.goTestResultSubjectSubscription = this.goService.observedSubjectTestResult.subscribe(
      testResult => { this.receivedTestResult(testResult); }
    );

    // Warning for when we get a prediction from the model, goService updated his TestResult object which has the prediction.
    this.goTestPredictionArrivedSubjectSubscription = this.goService.observedSubjectTestResultPosted.subscribe(
      testResult => { 
        this.showResultDiv = true;
        this.sendResponseToGecadSystem();    
      }
    );
    

    this.selectedGender = environment.gender.unknown;
    this.selectedAge = 0;    
    this.showAgeGenderSelector = true;
    this.showGonogoTestDiv = false;
    this.showResultDiv = false;
    this.getIp();
  }

  ngOnDestroy(): void {
    this.goTestResultSubjectSubscription.unsubscribe();
    this.goTestPredictionArrivedSubjectSubscription.unsubscribe();
  } 

  /**
   * Processes the receiving of a test result.
   * @param testResult Test Result received <GoTestResult>
   */
   public receivedTestResult(testResult : TestResult){        
    let myAge : number = 0;
    let myGender : string = environment.gender.unknown;
    this.showGonogoTestDiv = false;
    
    myAge = this.selectedAge;
    myGender = this.selectedGender;

    this.goService.getDirectModelPrediction(myAge, myGender);    
    console.log("Test result ready: ");
    console.log(testResult);
    this.showGonogoTestDiv = false;    
  }

  /** Sends the result to the Gecad System backend */
  public sendResponseToGecadSystem() {
    let idepa_url: string = environment.gecadbasicfatiguemodel.idepa_post_url;

    let body : GecadTest = new GecadTest();
    body.fatigueLevel = this.goService.lastTestResult.predictedFatigueLevel;
    body.ip = this.capturedIP;
    
    //https://github.com/angular/angular/issues/18586
    const myHttpOptions = {
      headers: new HttpHeaders({        
        
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
    };


    this.http.post<Response>(idepa_url, body , myHttpOptions).subscribe(
      {
        next: (response) => {
          console.log("Sucesefully posted GecadTest to idepa url.");          
        }
        , error: (error: any) => {
          console.log("Error trying to post GecadTest to idepa url ")
          console.log(error);
        }
        , complete: () => {
          console.log("Http post GecadTest on complete.");
        }
      }
    )

      
    
  }

  /** Try to get IP by calling an external site with a GET */
  public getIp(): void {
    let ip_get_url :string = environment.gecadbasicfatiguemodel.ip_get_url; //http://api.ipify.org/?format=json
    this.http.get(ip_get_url, { observe: 'response' }).subscribe(
      {
        next: (response : HttpResponse<Object>) => {
          console.log(response.body);
          if (response.body?.hasOwnProperty("ip")) {          
            type typeObjectWithIp = { ip: string };
            let myObjectWithIp = response.body as typeObjectWithIp;
            console.log(myObjectWithIp.ip);
            this.capturedIP = myObjectWithIp.ip;
          }
        }
        ,error: (error: any) => {
          console.log("ViewTestComponent Error getting IP through a Get.")
          console.log(error);
        }
        , complete: () => {
          console.log("The http GET from getIp() observable is now completed.");
        }
      }
    );
  }

  /** Submit button for the Age and Gender */
  public onSubmitAgeGender() : void {
    
       if(this.genderAgeForm != null && this.genderAgeForm != undefined)
       {
        this.selectedAge = Number(this.genderAgeForm.value.ageValueController);
        this.selectedGender = this.genderAgeForm.value.genderValueController;
        console.warn("Selected Gender: " + this.selectedGender + " Age: " + this.selectedAge); 
        this.showAgeGenderSelector = false;
        this.showGonogoTestDiv = true;
       }      
       else console.warn("onSubmitAgeGender called without valid genderAgeForm");
  }

}
