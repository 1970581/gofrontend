import { Component, OnDestroy, OnInit } from '@angular/core';
import { GoService } from '../Services/go.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-fatigue',
  templateUrl: './fatigue.component.html',
  styleUrls: ['./fatigue.component.css']
})
export class FatigueComponent implements OnInit, OnDestroy {

  
  public static readonly INVALID_START_FORM_LEVEL : number = 0;
  public static readonly MIN_FORM_LEVEL : number = 1;
  public static readonly MAX_FORM_LEVEL : number = 7;

  /** Form object for scale. More information on: //https://angular.io/guide/reactive-forms */
  scaleform : FormGroup ;

  /** If we sent the fatigue value to server */  
  public waitingFatiguePost : boolean = false;
  public fatiguePostedOk : boolean = false;

  /** go service subscription to receive warnings and results */
  public goPostedTestResultSubjectSubscription! : Subscription;



  constructor(private goService : GoService, translate: TranslateService) { 

    // Initializate the form
    this.scaleform = new FormGroup( 
      {
        scaleValueController : new FormControl( FatigueComponent.INVALID_START_FORM_LEVEL,
          [ // Form validators
            Validators.required,
            Validators.min(FatigueComponent.MIN_FORM_LEVEL),
            Validators.max(FatigueComponent.MAX_FORM_LEVEL)
          ])
      }
    );  
  }

  ngOnInit(): void {

    this.fatiguePostedOk = false;
    this.waitingFatiguePost = false;
    // Subscribe observer about posting results to backend with sucess.
    this.goPostedTestResultSubjectSubscription = this.goService.observedSubjectTestResultPosted.subscribe(
      sucess => {         
        this.fatiguePostedOk = sucess;        
        if(!sucess) this.waitingFatiguePost = false;        
      }
    );

    this.scaleform = new FormGroup( 
      {
        scaleValueController : new FormControl( FatigueComponent.INVALID_START_FORM_LEVEL,
          [ // Form validators
            Validators.required,
            Validators.min(FatigueComponent.MIN_FORM_LEVEL),
            Validators.max(FatigueComponent.MAX_FORM_LEVEL)
          ])
      }
    );  
  }

  ngOnDestroy(): void {
    this.goPostedTestResultSubjectSubscription.unsubscribe();
  }


    /** Called when you press the submit button on the bottom of the scale
   *  Passes the value of the fatigue scale to the Go service.
   */
     submitFatigue() {
       if (this.waitingFatiguePost) return;
       if(this.scaleform != null && this.scaleform != undefined)
       {
         console.warn("Submited fatige Level: " + this.scaleform.value.scaleValueController ); 
        let fatigueLevelAsNumber : number = Number(this.scaleform.value.scaleValueController);        
        this.waitingFatiguePost = true;
        this.goService.processNewFatigueScaleValue(fatigueLevelAsNumber);
        this.goService.submitTestResultToBackend();
       }      
    }   

}
