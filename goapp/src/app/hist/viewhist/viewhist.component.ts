import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoginService } from 'src/app/Services/login.service';
import { HttpClient, HttpParams, HttpResponse, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { TestResult } from 'src/app/modelo/testresult';
import { TranslateService } from '@ngx-translate/core';
import { TestResultDTO } from 'src/app/modelo/testresultdto';


@Component({
  selector: 'app-viewhist',
  templateUrl: './viewhist.component.html',
  styleUrls: ['./viewhist.component.css']
})
export class ViewhistComponent implements OnInit {

  /** Number of days to get as default */
  public static readonly DEFAULT_DAYS : number= 7;
  /** Test results gotten from backend */
  public testResults : TestResultDTO[] = [];

  constructor(public loginService: LoginService, private route: ActivatedRoute, private http: HttpClient,private translate : TranslateService) { }

  ngOnInit(): void {
    this.getTestResultsFromBackend(ViewhistComponent.DEFAULT_DAYS);
  }


  


  public getTestResultsFromBackend(days :number){

    let daysToAsk = days;
    if (daysToAsk <= 0) daysToAsk = 1;    
    let backend_url: string = environment.backend.backend_host + environment.backend.get_test_results + daysToAsk;
    
    let myUsername = this.loginService.myUser.username;
    let myToken = this.loginService.access_token;    

    //https://github.com/angular/angular/issues/18586
    const myHttpOptions = {
      headers: new HttpHeaders({
        'username': myUsername
        ,'token' : myToken
        //'Content-Type': 'application/x-www-form-urlencoded'
        //,"Access-Control-Allow-Origin": "*"
      }),
      responseType: 'json' as 'json',
      //withCredentials: true,        // WithCredentials sometimes makes CORS fail and is only needed for cookies.
      observe: 'response' as 'body'
    };


    this.http.get<HttpResponse<TestResult[]>>(backend_url, myHttpOptions).subscribe(    
      {
        next: (response) => {
          console.log("Sucesefully get of TestResults.");
          this.testResults = [];
          if(response.body != null){
            let results = response.body;
            for(let item of results)
            {
              let dto : TestResultDTO = new TestResultDTO();
              //dto.timestamp = item.getTimestampAsReadable();
              dto.timestamp = this.getTimestampAsReadable(item.timestamp);
              dto.goavg = item.goavg;
              dto.gofailnumber = item.gofailnumber;
              dto.nogofailnumber  = item.nogofailnumber;
              dto.selfFatigueLevel = "L" + item.selfFatigueLevel.toString();
              if(item.isRated) 
              {
                dto.predictedFatigueLevel = "L" + item.predictedFatigueLevel.toString();
                let desc : string = "";
                switch(item.predictedFatigueLevel)
                {
                  case 1: { desc = this.translate.instant("MODEL.L1"); break; }
                  case 2: { desc = this.translate.instant("MODEL.L2"); break; }
                  case 3: { desc = this.translate.instant("MODEL.L3"); break; }
                  case 4: { desc = this.translate.instant("MODEL.L4"); break; }
                  case 5: { desc = this.translate.instant("MODEL.L5"); break; }
                  case 6: { desc = this.translate.instant("MODEL.L6"); break; }
                  case 7: { desc = this.translate.instant("MODEL.L7"); break; }
                  break;
                  default: { desc = "?"; break;}
                }
                dto.fatigueLevelDescription = desc;
                //dto.predictedFatigueLevel = dto.predictedFatigueLevel + " " + desc;
              }
              this.testResults.push(dto);
            }

            // https://parallelcodes.com/typescript-how-to-sort-array-of-objects/
            this.testResults = this.testResults.sort((a, b) => (a.timestamp < b.timestamp) ? 1 : -1);


          }
        }
        , error: (error: any) => {
          console.log("Error trying to get TestResult from backend.")
          console.log(error);

          if (error instanceof HttpErrorResponse && error.status == 401) {
            console.error("Http post get Test Results Error. 401 Unauthorised.");
            let message: string = this.translate.instant("BACKEND.ERROR_442");    
            alert(message);
          } else {
            console.error("Http get Test Results Error. Unknown error.");
            let message: string = this.translate.instant("BACKEND.ERROR_UNKNOWN");    
            alert(message);
          }                    
        }
        , complete: () => {
          console.log("Http post Test Result on complete.");
        }
      }
    )

  }

      /** Returns a human readable version of timestamp for tables */
      public getTimestampAsReadable(time :string) : string{
        let answer :string = "error";

        
        try{

            let dateUTC :Date = new Date(time);    // From string UTC to UTC but wrong.
            
            let date :Date = new Date( Date.UTC(   // Convertion to true date using the UTC values.
              dateUTC.getFullYear(), 
              (1 + dateUTC.getMonth()),
              dateUTC.getDate(),
              dateUTC.getHours(),
              dateUTC.getMinutes(),
              dateUTC.getSeconds()
              ));

            // Build answer:
            answer = date.getFullYear().toString();
            answer = answer + "-" + (1 + date.getMonth()).toString().padStart(2, '0');;
            answer = answer + "-" + date.getDate().toString().toString().padStart(2, '0');
            answer = answer + " " + date.getHours().toString().padStart(2, '0');
            answer = answer + ":" + date.getMinutes().toString().padStart(2, '0');
            answer = answer + ":" + date.getSeconds().toString().padStart(2, '0');                       
                        
        }
        catch
        {
            //Do nothing.
        }
        
        return answer;
    }

    public selectChangeHandler(event : any) : void{
      console.log("ViewHist selected days: " + event.target.value);
      this.getTestResultsFromBackend(event.target.value);
    }

}
