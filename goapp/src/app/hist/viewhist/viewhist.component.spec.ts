import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewhistComponent } from './viewhist.component';

describe('ViewhistComponent', () => {
  let component: ViewhistComponent;
  let fixture: ComponentFixture<ViewhistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewhistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewhistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
