import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShownavbarComponent } from './shownavbar.component';

describe('ShownavbarComponent', () => {
  let component: ShownavbarComponent;
  let fixture: ComponentFixture<ShownavbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShownavbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShownavbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
