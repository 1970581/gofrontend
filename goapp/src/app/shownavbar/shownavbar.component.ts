import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '../Services/login.service';

@Component({
  selector: 'app-shownavbar',
  templateUrl: './shownavbar.component.html',
  styleUrls: ['./shownavbar.component.css']
})
export class ShownavbarComponent implements OnInit {

  constructor(public loginService: LoginService, translate : TranslateService) {}

  ngOnInit(): void {
  }

  public isAUserLogedIn(){
    return this.loginService.isAUserLogedIn();
  }
  
  /** Asks the login service to do the logout of the loged in user.  */
  public doLogout(){    
    this.loginService.doLogout();
  }

}
