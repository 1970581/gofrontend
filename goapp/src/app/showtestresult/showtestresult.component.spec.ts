import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowtestresultComponent } from './showtestresult.component';

describe('ShowtestresultComponent', () => {
  let component: ShowtestresultComponent;
  let fixture: ComponentFixture<ShowtestresultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowtestresultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowtestresultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
