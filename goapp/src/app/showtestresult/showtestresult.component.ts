import { Component, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { GoService } from '../Services/go.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-showtestresult',
  templateUrl: './showtestresult.component.html',
  styleUrls: ['./showtestresult.component.css']
})
export class ShowtestresultComponent implements OnInit, OnDestroy {

  /** For use of mathmatics in the html page */
  Math = Math;

  /** go service subscription to receive warnings and results */
  public goPostedTestResultSubjectSubscription! : Subscription;

  public fatiguePostedOk : boolean = false;

  constructor(public goService : GoService, translate: TranslateService) { }

  ngOnInit(): void {
    this.fatiguePostedOk = false;
    this.goPostedTestResultSubjectSubscription = this.goService.observedSubjectTestResultPosted.subscribe(
      sucess => {         
        this.fatiguePostedOk = sucess;

        // Update happens on go Service at submitTestResultToBackend()
      }
    );
  }

  ngOnDestroy(): void {
    this.goPostedTestResultSubjectSubscription.unsubscribe();
  }

}
