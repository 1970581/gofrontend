import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'goapp';

  constructor(translate: TranslateService) {
    translate.addLangs([ environment.language.languagePt, environment.language.languageEn]); 
    translate.use(environment.language.defaultLanguage);   
  }
}
