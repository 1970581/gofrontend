import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtestfatigueComponent } from './viewtestfatigue.component';

describe('ViewtestfatigueComponent', () => {
  let component: ViewtestfatigueComponent;
  let fixture: ComponentFixture<ViewtestfatigueComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewtestfatigueComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtestfatigueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
