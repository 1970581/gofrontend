import { Component, OnInit, OnDestroy } from '@angular/core';
import { GoService } from '../Services/go.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { TestResult } from '../modelo/testresult';
import { LoginService } from '../Services/login.service';

@Component({
  selector: 'app-viewtestfatigue',
  templateUrl: './viewtestfatigue.component.html',
  styleUrls: ['./viewtestfatigue.component.css']
})
export class ViewtestfatigueComponent implements OnInit,  OnDestroy{

  /** If we should show the gonogo test component HTML DIV */
  public showGonogoTestDiv: boolean = true;
  /** If we should show the fatigue questionare component HTML DIV */
  public showFatigueQuestionsDiv: boolean = false;
  /** If we should show the result */
  public showResultDiv: boolean = false;  

  public showRestart: boolean = false;

  /** go service subscription to receive warnings and results */
  public goTestResultSubjectSubscription! : Subscription; 
  public goPostedTestResultSubjectSubscription! : Subscription

  constructor(public loginService: LoginService,
    private goService : GoService,
    private route: ActivatedRoute,
    private router: Router   
  ) {} 

  ngOnInit() {
    this.showGonogoTestDiv = true;
    this.showFatigueQuestionsDiv = false;
    this.showResultDiv = false; 
    this.showRestart = false;   
    

    this.goTestResultSubjectSubscription = this.goService.observedSubjectTestResult.subscribe(
      testResult => { this.receivedTestResult(testResult); }
    );
    this.goPostedTestResultSubjectSubscription = this.goService.observedSubjectTestResultPosted.subscribe(
      sucess => { 
        if (sucess){
          this.showRestart = true;
        }
        else {
          this.showRestart = true;
        }        
      }
    );
    this.showGonogoTestDiv = true;
    this.showFatigueQuestionsDiv = false;
    this.showResultDiv = false;
  }

  ngOnDestroy(): void {
    this.goTestResultSubjectSubscription.unsubscribe();
    this.goPostedTestResultSubjectSubscription.unsubscribe();
  } 


  /**
 * Processes the receiving of a test result.
 * @param testResult Test Result received <GoTestResult>
 */
  public receivedTestResult(testResult: TestResult) {
    this.showGonogoTestDiv = false;
    this.showResultDiv = true;
    this.showFatigueQuestionsDiv = true;    
  }  

  

  public restartTest(){

    console.log("Restart testfatigue");

    this.showGonogoTestDiv = true;
    this.showFatigueQuestionsDiv = false;
    this.showResultDiv = false; 
    this.showRestart = false;
    //this.ngOnDestroy();
    //this.ngOnInit();
    /*
    this.router. navigateByUrl('/testfatigue', 
    { 
      skipLocationChange: true
    }
    );
    */
  }

}
