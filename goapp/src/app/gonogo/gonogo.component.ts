import { Component, OnDestroy, OnInit } from '@angular/core';
import { HostListener } from "@angular/core"; // for key press detection
import { ViewChild, ElementRef } from '@angular/core';
import { timeout } from 'rxjs/operators';
import { NumberFormatStyle } from '@angular/common';
import { GoService } from '../Services/go.service';
import { ScoreItem } from '../modelo/scoreitem';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';
import { TestParameters } from '../modelo/testparameters';

@Component({
  selector: 'app-gonogo',
  templateUrl: './gonogo.component.html',
  styleUrls: ['./gonogo.component.css']
})
export class GonogoComponent implements OnInit , OnDestroy{

  /** This is where we detect that the space key was pressed.
  * https://angular.io/api/core/HostListener
  * @param event the keydown event
  */
  @HostListener('window:keydown', ['$event'])
  onKeyDown(event : KeyboardEvent) {
    // NOTA browsers mais velhos podem necessitar de codigo legacy aqui para reconhecer o Key Code.
    //if (event.keyCode == 32) { this.spacePress(); }    
    // https://stackoverflow.com/questions/35394937/keyboardevent-keycode-deprecated-what-does-this-mean-in-practice
    if (event.key !== undefined  && event.key == " ") this.spacePress();       
  }

  //Note: The canvas tag is not supported in Internet Explorer 8 and earlier versions
  //https://stackoverflow.com/questions/44426939/how-to-use-canvas-in-angular2
  @ViewChild('canvas', { static: true }) canvas: ElementRef<HTMLCanvasElement> | null;//= ElementRef.prototype;//new ElementRef.prototype <HTMLCanvasElement>();
  private ctx: CanvasRenderingContext2D  | null ;

  //Preload Images:
  imageInstructions: HTMLImageElement = new Image();
  imageGo: HTMLImageElement = new Image();
  imageNoGo: HTMLImageElement = new Image();
  imageGoFail: HTMLImageElement = new Image();
  imageNoGoFail: HTMLImageElement = new Image();
  imageEnd: HTMLImageElement = new Image();  
  imageGoFailPressToSoon: HTMLImageElement = new Image();

  //GonoGo variables.
  //public numberOfTrials : number = environment.defaultTestParameters.numberOfTrials; //20;    
  public numberOfTrials : number = TestParameters.numberOfTrials;
  //public numberOfNoGoTrials : number = environment.defaultTestParameters.numberOfNoGoTrials; // 4;  // number of NoGo should be less than numberOfTrials
  public numberOfNoGoTrials : number = TestParameters.numberOfNoGoTrials;
  public numberOfTrialsDone : number = 0;  
  public isGoArray : Array<boolean> = new Array<boolean>(); // Used to know if should be a go or a no-go
  public myTimeout : any;  // Timeout variable storage   

    // States of the go no go testing.
  public myState : string = "INITIAL_STATE_ZERO";
  public static readonly STATE_ZERO : string = "INITIAL_STATE_ZERO";
  public static readonly STATE_INSTRUCTION : string = "SHOWING_INSTRUCTIONS";
  public static readonly STATE_WAITING_GO : string = "WAITING_GO";
  public static readonly STATE_WAITING_NOGO : string = "WAITING_NOGO";
  public static readonly STATE_PROCESSING : string = "PROCESSING";
  public static readonly STATE_FINISHED : string = "FINISHED";
  public static readonly STATE_WAITING_FOR_NEXT_TRIAL : string = "WAITING_FOR_NEXT_TRIAL";

  //Wait times for images and cases test.
  public static readonly WAIT_TIME_TO_SHOW_INSTRUCTIONS : number = environment.defaultTestParameters.image.WAIT_TIME_TO_SHOW_INSTRUCTIONS; // 2000 ms
  public static readonly WAIT_TIME_TO_DISPLAY_FAIL : number = environment.defaultTestParameters.image.WAIT_TIME_TO_DISPLAY_FAIL // 2000 ms

  //public static readonly WAIT_TIME_TO_FAIL_GO_CASE : number = environment.defaultTestParameters.image.WAIT_TIME_TO_FAIL_GO_CASE //500 ms
  public static readonly WAIT_TIME_TO_FAIL_GO_CASE : number = TestParameters.waitTimeToFailGo;
  //public static readonly WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON : number = environment.defaultTestParameters.image.WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON // 100 ms
  public static readonly WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON : number = TestParameters.waitTimeToFailGoPressToSoon;
  //public static readonly WAIT_TIME_TO_PASS_NOGO_CASE : number = environment.defaultTestParameters.image.WAIT_TIME_TO_PASS_NOGO_CASE // 2000 ms  
  public static readonly WAIT_TIME_TO_PASS_NOGO_CASE : number = TestParameters.waitTimeToPassNoGo;
  //public static readonly WAIT_TIME_BETWIN_TRIALS : number = environment.defaultTestParameters.image.WAIT_TIME_BETWIN_TRIALS // 500 ms
  public static readonly WAIT_TIME_BETWIN_TRIALS : number = TestParameters.waitTimeBetweenTrials;

  //time & scoring 
  //https://stackoverflow.com/questions/313893/how-to-measure-time-taken-by-a-function-to-execute
  //https://developer.mozilla.org/en-US/docs/Web/API/Performance/now  
  public timeStartTime : number = 0;
  public timeGoTime : number = 0;  
  public score: Array<ScoreItem> = new Array<ScoreItem>();
  public static readonly SCORE_GO_FAIL_TIME_TO_RECORD : number = environment.defaultTestParameters.SCORE_GO_FAIL_TIME_TO_RECORD;  //2000;
  public static readonly SCORE_NOGO_TIME_TO_RECORD :number = environment.defaultTestParameters.SCORE_NOGO_TIME_TO_RECORD; //2000; 

    // This values must be exact in GonogoComponent and in the GoService to avoid Circular dependencys.
  // If changed here you must also change in the other. 
  public static readonly SCORE_GO_TRIAL_TYPE : string = environment.defaultTestParameters.name_go; //"go";
  public static readonly SCORE_NOGO_TRIAL_TYPE : string = environment.defaultTestParameters.name_nogo; //"nogo";
  public static readonly SCORE_TRIAL_FAIL: boolean = environment.defaultTestParameters.trial_fail; // true
  public static readonly SCORE_TRIAL_PASS: boolean = environment.defaultTestParameters.trial_pass; // pass
  public scoreAverageValue :number = 0;

  public readonly GONOGO_IMAGES_DIRECTORY: string = "./assets/gonogo_images/";

  constructor( private goService : GoService , private translate : TranslateService) {
    this.canvas = null;
    this.ctx = null;
   }

  ngOnInit(): void {
    if (this.canvas != null)
    this.ctx = this.canvas.nativeElement.getContext('2d'); //Obtain canvas context to able to draw
    
    //Schedule the start of test, it needs a delay to ensure the images were loaded.
    this.myTimeout = setTimeout(() => {
      this.initiateTest();
        this.myState = GonogoComponent.STATE_INSTRUCTION; }
        , GonogoComponent.WAIT_TIME_TO_SHOW_INSTRUCTIONS
       ); 

      // Register event for space key press
      /* DO NOT USE THIS WAY OR ELSE WE CANT REMOVE THE handler on destroy and the class keeps forever in memory.
      window.addEventListener('keypress', (e) => {
          if (e.keyCode == 32) { this.spacePress();}
        }, false
      );
      */
  }

  ngOnDestroy(): void {
    clearTimeout(this.myTimeout);    
    //console.log("On Destroy called.");
  }

  ngAfterViewInit(): void {
    //console.log("Go/no-Go: ngAfterViewInit()"); 

    let lang_extention :string = environment.language.defaultLanguage;
    if(this.translate.currentLang != null && this.translate.currentLang != undefined) lang_extention = this.translate.currentLang;

    // Load the images to objects at init so that they load faster.
    this.imageInstructions.src =  this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_instructions.png";
    this.imageGo.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_go.png";
    this.imageNoGo.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_nogo.png";
    this.imageGoFail.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_failgo.png";    
    this.imageNoGoFail.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_failnogo.png";  
    this.imageEnd.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_end.png"; 
    this.imageGoFailPressToSoon.src = this.GONOGO_IMAGES_DIRECTORY + lang_extention + "/isep_failgo_by_guessing.png";
  }   

  /** Process click on the screen/canvas. */
  public clickOnCanvas() {    
    console.log("Go/no-Go: Clicked on image. State: " + this.myState);
  }

  /** User presses on space, depending on state jumps to correct method */
  public spacePress() {
    // TODO: Switch case
    if (this.myState == GonogoComponent.STATE_INSTRUCTION) this.waitTimeForNextTrial();
    if (this.myState == GonogoComponent.STATE_WAITING_GO) this.passGo();
    if (this.myState == GonogoComponent.STATE_WAITING_NOGO) this.failNoGo();
    if (this.myState == GonogoComponent.STATE_FINISHED) console.log("finished");
  }

  /** Starts the test,  */
  public initiateTest() {
    this.myState = GonogoComponent.STATE_INSTRUCTION;
    //this.canvas.nativeElement.focus();  //Doesn't work on canvas. // Prevents the space from trigering buttons like a restart button.
    this.drawInstructions();
    this.restartScore();
  }

      // Draw Methods.
  public drawInstructions() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageInstructions, 400, 100);}
  public drawGo() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageGo, 400, 200);   }
  public drawNoGo() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageNoGo, 400, 200);  }
  public drawGoFail() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageGoFail, 400, 200);  }
  public drawGoFailPressToSoon() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageGoFailPressToSoon, 400, 200);  }
  public drawNoGoFail() { this.drawClearCanvas(); if(this.ctx != null) this.ctx.drawImage(this.imageNoGoFail, 400, 200);  }
  public drawClearCanvas(){ 
    if(this.ctx != null && this.canvas != null) {
    this.ctx.clearRect(0,0, this.canvas.nativeElement.width, this.canvas.nativeElement.height);}}
  public drawEnd() { this.drawClearCanvas(); if (this.ctx != null) this.ctx.drawImage(this.imageEnd, 400, 200);  }

  /** Starts a trial */
  private nextGoNoGo() {
    clearTimeout(this.myTimeout);
    if (this.numberOfTrialsDone >= this.numberOfTrials) {
      this.endTest();
      return;
    }
    this.numberOfTrialsDone = this.numberOfTrialsDone + 1;
    console.log("Trial: " + this.numberOfTrialsDone);

    let isGo: boolean = this.isGo();
    if (isGo) {   // GO test
      this.drawGo();
      this.timeStartTime = performance.now(); // record start time.      
      this.myState = GonogoComponent.STATE_WAITING_GO;
      this.myTimeout = setTimeout(() => {
        this.failGo();
      }
        , GonogoComponent.WAIT_TIME_TO_FAIL_GO_CASE);

    } else {    // NoGO test      
      this.drawNoGo();
      this.myState = GonogoComponent.STATE_WAITING_NOGO;
      this.myTimeout = setTimeout(() => {
        this.passNoGo();
      }
        , GonogoComponent.WAIT_TIME_TO_PASS_NOGO_CASE);
    }

  }

  /** trial of Go ended in a pass */
  public passGo() {
    this.myState = GonogoComponent.STATE_PROCESSING;
    this.timeGoTime = performance.now();    // Record Go Time.    
    clearTimeout(this.myTimeout);
    let timeReaction: number = (this.timeGoTime - this.timeStartTime);

    // If user pressed to soon, we should fail the Go trial because he hasn't had enough time to react.
    if (timeReaction < GonogoComponent.WAIT_TIME_TO_FAIL_GO_CASE_BY_PRESS_TO_SOON) {
      this.failGoPressToSoon(timeReaction);
    }
    else {
      this.addScoreItem(GonogoComponent.SCORE_GO_TRIAL_TYPE, timeReaction, GonogoComponent.SCORE_TRIAL_PASS);
      console.log("GO pass! " + timeReaction);
      this.waitTimeForNextTrial();
    }
  }

  /** 
   * trial of Go failed due to Press to soon in a Go trial, heppens when the user is
   *  presented with the go signal but hasn't passed enought time (100 ms) for him to react but space is pressed.
   */
   public failGoPressToSoon(timeReaction:number){
    this.drawGoFailPressToSoon();
    this.addScoreItem( GonogoComponent.SCORE_GO_TRIAL_TYPE, timeReaction, GonogoComponent.SCORE_TRIAL_FAIL );
    console.log("GO fail by press to soon! " + timeReaction);
    this.myTimeout = setTimeout(() => {
      this.waitTimeForNextTrial();}
      , GonogoComponent.WAIT_TIME_TO_DISPLAY_FAIL);
  }

    /** trial of Go ended in fail by the timout function being called to run this */
  public failGo(){ 
    this.myState = GonogoComponent.STATE_PROCESSING;    
    //timeout is already dead.
    this.drawGoFail();    
    this.addScoreItem( GonogoComponent.SCORE_GO_TRIAL_TYPE, GonogoComponent.SCORE_GO_FAIL_TIME_TO_RECORD, GonogoComponent.SCORE_TRIAL_FAIL );
    console.log("GO fail!");
    this.myTimeout = setTimeout(() => {
      this.waitTimeForNextTrial();}
      , GonogoComponent.WAIT_TIME_TO_DISPLAY_FAIL);    
  } 

  /** trial of Nogo ended in pass by the timeout function being called to run this */
  public passNoGo(){
    this.myState = GonogoComponent.STATE_PROCESSING;
    //clearTimeout(this.myTimeout); // timout already dead.
    this.addScoreItem( GonogoComponent.SCORE_NOGO_TRIAL_TYPE, GonogoComponent.SCORE_NOGO_TIME_TO_RECORD, GonogoComponent.SCORE_TRIAL_PASS );
    console.log("NoGO pass!");
    this.waitTimeForNextTrial();
  }

  /** trial of Nogo ended in failure by key press  */
  public failNoGo(){
    this.myState = GonogoComponent.STATE_PROCESSING;
    clearTimeout(this.myTimeout);
    this.addScoreItem( GonogoComponent.SCORE_NOGO_TRIAL_TYPE, GonogoComponent.SCORE_NOGO_TIME_TO_RECORD, GonogoComponent.SCORE_TRIAL_FAIL );
    this.drawNoGoFail();    
    console.log("NoGO fail!");
    this.myTimeout = setTimeout(() => {
      this.waitTimeForNextTrial();}
      , GonogoComponent.WAIT_TIME_TO_DISPLAY_FAIL);
  }

  /** Clears canvas and sets the wait time to start the next trial. */
  public waitTimeForNextTrial(){
    this.myState = GonogoComponent.STATE_WAITING_FOR_NEXT_TRIAL;
    this.drawClearCanvas();
    this.myTimeout = setTimeout(() => {
      this.nextGoNoGo();}
      , GonogoComponent.WAIT_TIME_BETWIN_TRIALS);
  }

  /** Returns if the trial is a Go trial (true), and removes it from isGoArray */
  private isGo() : boolean {
    let poped = this.isGoArray.pop();
    if(poped != undefined) return poped;
    else throw 'this.isGoArray.pop() returned undifined.';
  }
  
  /** Method called at the end of all the trials. Submits score.  */
  private endTest(){
    this.myState = GonogoComponent.STATE_FINISHED;   
    this.drawEnd();    
    this.calculateScoreAverage();

    // Pass the test result to the GoService.
    this.goService.processNewScore(this.score);

  }

  /** Restarts the score and the isGoArray. */
  private restartScore(){
    this.numberOfTrialsDone = 0;
    if(this.numberOfNoGoTrials > this.numberOfTrials) this.numberOfNoGoTrials = this.numberOfTrials;

    //https://www.tutorialsteacher.com/typescript/typescript-array
    this.score = new Array<ScoreItem>();
    this.isGoArray = new Array<boolean>();
    
    // build isGoArray
    for (let i = 0; i < this.numberOfTrials; i++){
      this.isGoArray.push(true); //fill with go-true
    }
    for (let i = 0; i < this.numberOfNoGoTrials; i++){
      this.isGoArray[i] = false; //fill the firsts ? with NoGo-false
    }
    this.shuffleIsGoArray();  // shuffle the array, so nogo location are random and not set.
  }
  
  /** Shuffles the Go and No-go in the boolean array. */
  private shuffleIsGoArray(){
    let otherPosition : number = 0;
    let initial, other : boolean;
    for (let i = 0; i < this.isGoArray.length; i++){
      otherPosition = Math.floor(Math.random() * this.isGoArray.length);
      initial = this.isGoArray[i];
      other = this.isGoArray[otherPosition];
      this.isGoArray[i] = other;
      this.isGoArray[otherPosition] = initial;
    }
    console.log(this.isGoArray.toString());
  }

  /**
   * Adds the result of a trial to the local score (ScoreItem)
   * @param trialType type of trial
   * @param trialTime duration of the trial
   * @param trialIsFail if the trial failed
   */
  public addScoreItem(trialType : string, trialTime : number, trialIsFail : boolean){
    let newScoreItem : ScoreItem = new ScoreItem();
    newScoreItem.type = trialType;
    newScoreItem.time = trialTime;
    newScoreItem.isFail = trialIsFail;
    this.score.push(newScoreItem);
  }

  /** Local average finding. No longer relevant except for debug. Check the GoService. */
  public calculateScoreAverage() : number {
    if (this.score == null || this.score == undefined || this.score.length == 0) return 0;
    let average : number = 0;
    let denominator : number = 0;
    for (let i = 0; i < this.score.length; i++){
      if(this.score[i].type == GonogoComponent.SCORE_GO_TRIAL_TYPE 
        && this.score[i].isFail == GonogoComponent.SCORE_TRIAL_PASS){
          average = average + this.score[i].time;
          denominator = denominator +1;
        }
    }
    if (denominator <= 0) return 0;
    average = Math.floor(average / denominator);  
    this.scoreAverageValue = average;
    return average;
  }

}
