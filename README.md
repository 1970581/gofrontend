# README #

This is the front end of gonogoapp and fatigue detection application.        


### Something about the project: ###

The project is composed by several repositories:            
This frontend: https://bitbucket.org/1970581/gofrontend       
The backend: https://bitbucket.org/1970581/backend      
The user management using KeyCloack: https://bitbucket.org/1970581/keycloak           
The Machine Learning: https://bitbucket.org/1970581/mlfinal

The whole system when built looked like this:           
![Implantation Diagram](implantation.PNG "Implantation Diagram")

A anonymous standalone docker of the frontend and prediction algoritm can be found here: https://bitbucket.org/1970581/dockerstandalone      

## About Frontend ##

The aplication was developed in Angular.io using the cli line command interface.
It is best run with:              
* - Angular CLI: 13.2.5          
* - Node: 16.14.0               
* - Package Manager: npm 8.3.1                  
* - OS: linux x64                    

To run the app just clone this repository, install/update the dependencies, and run with the command:           
> npm install

> ng serve 


In our server we use a service to run this frontend which use the following command:
> ng serve --host 0.0.0.0 --port 443 --disable-host-check --ssl --ssl-cert /etc/letsencrypt/live/gonogoapp.org/fullchain.pem --ssl-key /etc/letsencrypt/live/gonogoapp.org/privkey.pem

You might require your own TSL certificates and Firewall configuration.     
The service can be seen inside directory servico.

Or you can use something like Nginx to serve the built code, like we do in the Docker version.

## Configuration ##

The config file of this application is:
> environment.ts

Configuration options are mostly explicit. User block refers to authentication with the Keycloak user management application.
The rgpd_consent_ok is the string content of a user rgdp property that indicates that he has sign the rgpd agreement.

Backend is the connections to the backend that saves and gets the gonogo result information.

defaultTestParameters are the Go/NoGo test parameters. Subsection image are the wait times related with image presentations and for accounting fails.

fatigue only has the default fatigue level. Fatigue scale is present in the translation files.

gecadbasicfatiguemodel refers to the options related to the route \test made avaiable to be used at IDEPA presentation. It contained a function to rate fatigue based on a test result, a prototype that was replaced by a http call to the final model based on machine learning. It also contains the translations for the fatigue levels for IDEPA. The "idepa_post_url" is the url to foward the prediction result.
The "direct_prediction_model_url@ is the url of the fatigue model being used for IDEPA.




