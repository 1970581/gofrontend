[Unit]
Description=goapp
After=network-online.target

[Service]
Restart=on-failure
WorkingDirectory=/opt/goapp/gofrontend/goapp/
ExecStart=/usr/lib/node_modules/@angular/cli/bin/ng.js serve --host 0.0.0.0 --port 443 --disable-host-check --ssl --ssl-cert /etc/letsencrypt/live/gonogoapp.org/fullchain.pem --ssl-key /etc/letsencrypt/live/gonogoapp.org/privkey.pem

[Install]
WantedBy=multi-user.target